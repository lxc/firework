\documentclass[table]{article} % For LaTeX2e
\usepackage{times,nips14submit_e}
\usepackage{hyperref}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{marvosym}
\usepackage{subfigure}
\usepackage{multirow,bigstrut}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{dsfont}
\usepackage{flushend}
\usepackage{url}
%\documentstyle[nips13submit_09,times,art10]{article} % For LaTeX 2.09
% Additional package added
\usepackage{booktabs}
\usepackage{xcolor}
\usepackage{multirow}
\usepackage[font=small,labelfont=bf]{caption}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{color}

\nipsfinalcopy % Uncomment for camera-ready version

\title{Firework Animation by Particle System}

\author{
Xiaochen Lian \\
University of California, Los Angeles\\
\texttt{lianxiaochen@ucla.edu} \\
\AND
Xianjie Chen \\
University of California, Los Angeles\\
\texttt{cxj@ucla.edu}}

\begin{document}
\maketitle

\section{Introduction}
In this report, we present our method to generate realistic and beautiful firework animation based on physically based modeling, and motion blur simulation. More precisely, we specify a particle system for each firework which exploits the physical laws each particle should follow. Moreover, we make our animation more beautiful by designing different user defined styles (e.g., explosion styles, color chaining styles etc.), and also a simulation of motion blur. We present our animation result by a video that's available online.

\section{Task}
Our task is to generate \emph{realistic} and beautiful firework animation. Figure~\ref{fig:goal} shows some examples of animation results.

\begin{figure}[H]
\centering
  \includegraphics[width=\linewidth]{figures/goals}
  \caption{Examples of firework animation results. To make our animation of firework more beautiful, we design different explosion styles, color changing styles etc.}
  \label{fig:goal}
\end{figure}

\section{The Particle System}
We model each firework by a particle system. Each firework consists of a set of particles. We generate the animation of the firework by animating each particle of the firework. Each particle in the system has both physical attributes (e.g., size, position, velocity, brightness etc.) and style attributes (e.g., color, size, etc). The physical attributes should follow the physical laws to ensure that our animation is realistic. The style attributes are designed by user to make the animation diverse and beautiful. 

\subsection{Physical Laws}
In this section, we introduce the physical laws we follow in our animation.  

\textbf{Newton's laws of motion:} 
The motion in our animation of firework is of low speed. Therefore, it should follow the  classical mechanics. The motion of each particle is governed by the familiar $\mathbf{f} = m\mathbf{a}$. Most of the time, each particle moves independently, except when the explosion happens. We consider the \emph{Gravity} and \emph{Viscous Drag Force} in our animation: 
\begin{eqnarray}
f_g &=& mg \\
f_d &=& \frac{1}{2} \rho \times Av^2 ,
\end{eqnarray}
\noindent where $m$ is the mass of  the particle, $g$ is the gravity of earth, $\rho$ is the coefficient of damping, $A$ is the cross-sectional area of the particle, and $v$ is the current velocity of the particle.

Figure~\ref{fig:force} summarize the forces on each particle. The animation is generated frame by frame, we calculate the new positions of each particles based on the Newton's laws of motion. Node that the forces change with the velocity of each particle. We approximate the forces between each pair of neighboring frames by using the velocity at the previous frame. This is an reasonable approximation under the condition of slow motion and small time gap between neighboring frames, which is certainly the case in firework animation. 

\begin{figure}[H]
\centering
  \includegraphics[width=0.2\linewidth]{figures/forces}
  \caption{Forces on each particle.}
  \label{fig:force}
\end{figure} 

\textbf{Conservation of Momentum:} 
When the firework explodes, the velocity of each particle changes suddenly. However, the overall momentum of the particles in each firework should remain the same, since explosion is caused by internal forces. Formally, 

\begin{equation}
p = mv = \sum_{i} m_i v_i
\end{equation}

All the explosion styles we designed follow the conservation of momentum.

\textbf{Brightness Fading:}
In reality, the fuel of firework is limited. After explosion the brightness of each particle should fade. We use a linear model to simulate this kind of fading effect, that is the brightness of each particle decreases linearly. This is done by changing the alpha value of each particle.

\subsection{Different Style}

We make our animation diverse and beautiful by varying the style attributes of each particles. In our animation, we use the following user defined styles:

\textbf{Color styles:} Each particle has its initial color and the color changes smoothly after the firework explodes. 

\textbf{Particle size:} We design fireworks with different particle sizes. The difference in the size results in different motion effect, since the drag force is related to particle's size.

\textbf{Fuel capacity:} This is related to the change of particle's brightness.

\textbf{Explosion style:} We design some explosion styles that agree with physical laws. Figure~\ref{fig:explosion} illustrates some explosion styles we designed.
 \begin{figure}[H]
\centering
  \includegraphics[width=\linewidth]{figures/explosion}
  \caption{Examples of explosion styles we designed.}
  \label{fig:explosion}
\end{figure}

\section{Simulation of Tails}

In this section, we describe how to simulate the tail effect of fireworks. Normally, we compute the positions of particles and render them to an image, as we discussed above. This will just create an animation with colorful points moving. To simulate the tail effect, we blend the current frame with previous frames, i.e. computing the weighted sum of current frame and previous frames. The weights decrease as time goes by. More specifically, let $f_i$ be the 2-D image created according to particles' movement and $D_i$ be the actual frame rendered on the screen at time $i$. For $i=1$, we have $D_i = f_i$, and for $i>1$, $D_i = f_i + \alpha*D_{i=1}$, where $\alpha$ is decreasing rate of visual visibility of the frames. Figure \ref{fig:tail} illustrates this idea.

\begin{figure}[H]
\centering
    \includegraphics[width=\textwidth]{figures/tail_idea.pdf}
    \caption{Illustration of the simulation of tails. The movement of three particles are rendered, as shown in the first row. The second row shows how blending could be used to simulate tails.} \label{fig:tail}
\end{figure}

Implementing this idea through OpenGL is not straightforward. In OpenGL rendering pipeline, the geometry data and textures are transformed and passed several tests, and then finally rendered onto a screen as 2D pixels. The final rendering destination of the OpenGL pipeline is called window-system-provided framebuffer\footnote{Framebuffer is a collection of 2D arrays or storages utilized by OpenGL; colour buffers, depth buffer, stencil buffer and accumulation buffer.}. To blend a 2-D image with previous frames before putting it onto screen, we need to create an object similar to the framebuffer in the memory, where the previous blended result is stored. 

In the following, we introduce the technique called render to texture, which utilizes the application-created framebuffer to address this problem.

\subsection{Frame Buffer Object}
In OpenGL rendering pipeline, the geometry data and textures are transformed and passed several tests, and then finally rendered onto a screen as 2D pixels. The final rendering destination of the OpenGL pipeline is called framebuffer. Framebuffer is a collection of 2D arrays or storages utilized by OpenGL; colour buffers, depth buffer, stencil buffer and accumulation buffer. By default, OpenGL uses the framebuffer as a rendering destination that is created and managed entirely by the window system. This default framebuffer is called window-system-provided framebuffer.

The OpenGL extension, \texttt{GL\char`_ARB\char`_framebuffer\char`_object} provides an interface to create additional non-displayable framebuffer objects (FBO). This framebuffer is called application-created framebuffer in order to distinguish from the default window-system-provided framebuffer. By using framebuffer object (FBO), an OpenGL application can redirect the rendering output to the application-created framebuffer object (FBO) other than the traditional window-system-provided framebuffer.

A FBO contains a collection of rendering destinations; color, depth and stencil buffer. These logical buffers in a FBO are called framebuffer-attachable images, which are 2D arrays of pixels that can be attached to a framebuffer object. There are two types of framebuffer-attachable images; texture images and renderbuffer images. If an image of a texture object is attached to a framebuffer, OpenGL performs ''render to texture". And if an image of a renderbuffer object is attached to a framebuffer, then OpenGL performs "offscreen rendering". Multiple texture objects or renderbuffer objects can be attached to a framebuffer object through the attachment points.

There are multiple color attachment points (\texttt{GL\char`_COLOR\char`_ATTACHMENT0}, \ldots, \texttt{GL\char`_COLOR\char`_ATTACHMENTn}), one depth attachment point (\texttt{GL\char`_DEPTH\char`_ATTACHMENT}), and one stencil attachment point (\texttt{GL\char`_STENCIL\char`_ATTACHMENT}) in a framebuffer object. The number of color attachment points is implementation dependent, but each FBO must have at least one color attachement point. Notice that the framebuffer object itself does not have any image storage(array) in it, but, it has only multiple attachment points.

Framebuffer object (FBO) provides an efficient switching mechanism; detach the previous framebuffer-attachable image from a FBO, and attach a new framebuffer-attachable image to the FBO, which is much faster than switching between FBOs. FBO provides \texttt{glFramebufferTexture2D()} to switch 2D texture objects, and \texttt{glFramebufferRenderbuffer()} to switch renderbuffer objects.

\subsection{Render to Texture}
Sometimes, you need to generate dynamic textures on the fly. The most common examples are generating mirroring/reflection effects, dynamic cube/environment maps and shadow maps. Dynamic texturing can be accomplished by rendering the scene to a texture. Using FBO, we can render a scene directly onto a texture, so we don't have to use the window-system-provided framebuffer at all. This technique is called rendering to a texture. By binding to a FBO the output of a GLSL fragment shader is drawn onto a texture instead of directly to the screen. Once your graphics is output on the texture you can re-pass it back into a shader program for further processing, or bind it to different geometry. Figure \ref{fig:r2t} shows the pipeline of render to texture.

\begin{figure}[t]
\centering
    \includegraphics[width=\textwidth]{figures/render2texture.pdf}
    \caption{The pipeline of render to texture using Frame Buffer Object.} \label{fig:r2t}
\end{figure}

\subsection{Algorithm}

We create an FBO with a texture object and a renderbuffer object. The texture object is our per-frame color buffer where current frame is rendered. We attach it to \texttt{GL\char`_COLOR\char`_ATTACHMENT0}. The renderbuffer object is used as accumulation buffer, i.e. $D_i$, which stores the actual content to be displayed on the screen. We attach it to \texttt{GL\char`_COLOR\char`_ATTACHMENT0}.
Our tail simulation algorithm consists of three steps:
\begin{enumerate}
\item Render firework particles to the per-frame color buffer;\\
\item Draw a quad that fills the screen onto (with blending) the accumulation buffer, textured with the color buffer;
\item Copy the pixels in the accumulation buffer to the default framebuffer, i.e. the screen.
\end{enumerate}


\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=L,
  xleftmargin=\parindent,
  language=C++,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{blue},
  commentstyle=\itshape\color{mygreen},
  identifierstyle=\color{black},
  stringstyle=\color{mymauve},
}
\lstset{escapechar=@,style=customc}

\begin{lstlisting}
/************************ Step One******************/
glBindFramebuffer(GL_FRAMEBUFFER, fboId);        
// switch to GL_COLOR_ATTACHMENT0 to 
// render current frame
glDrawBuffer(GL_COLOR_ATTACHMENT0);
// draw fireworks
drawScene(window);
glBindFramebuffer(GL_FRAMEBUFFER, 0);

/************************ Step Two******************/
// switch to GL_COLOR_ATTACHMENT1 as 
// rendering destination
glDrawBuffer(GL_COLOR_ATTACHMENT1);
...
// blending with blend_weight
glBlendColor(0.0, 0.0, 0.0, blend_weight);
glBlendFunc(GL_SRC_ALPHA,GL_CONSTANT_ALPHA);
glBlendEquation(GL_FUNC_ADD);
glBindTexture(GL_TEXTURE_2D, textureId);
// draw quad textured with current frame
glBegin(GL_QUADS);
glTexCoord2f(0,0);
glVertex2f( 0.0f, windowHeight);
...
glEnd();
glBindTexture(GL_TEXTURE_2D, 0);

/************************ Step Three******************/
// set GL_COLOR_ATTACHMENT1 as source
glBindFramebuffer(GL_READ_FRAMEBUFFER,fboId);
glReadBuffer(GL_COLOR_ATTACHMENT1);
// set screen buffer as destination        
glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
// copy from source to destination and display
glBlitFramebuffer(...);
glBindFramebuffer(GL_FRAMEBUFFER, 0);

\end{lstlisting}

\end{document}


