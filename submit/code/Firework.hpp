#ifndef FIREWORK_H
#define FIREWORK_H

#include <cstdlib>
#include <cmath>
#include <vector>
#include <algorithm>
#include <OpenGL/gl.h>
#include <GLFW/glfw3.h>    // Include OpenGL Framework library

extern const GLfloat DRAG_RO;
extern const GLfloat GRAVITY;
extern const GLfloat baselineYSpeed;
extern const GLfloat maxYSpeed;
extern const GLfloat PI;

extern GLint windowWidth;
extern GLint windowHeight;

std::vector<GLfloat> sampleFrom(int n, GLfloat a, GLfloat b);
GLfloat sampleFrom(GLfloat a, GLfloat b);
int sampleInt(int n); // [1,n]

// BANG factory
struct BangInfo {
	std::vector<GLfloat> vPool;
	std::vector<GLfloat> XYAPool;
	std::vector<GLfloat> XZAPool;

	std::vector<GLfloat> RcrPool;
	std::vector<GLfloat> GcrPool;
	std::vector<GLfloat> BcrPool;
	std::vector<GLfloat> AcrPool;
};
class BangFactory {
private:
	static std::vector<GLfloat> velShop(int n, int kind = 0);		// velocity shop
	static std::vector<GLfloat> angShop(int n, int kind = 0); 		// angle shop
	static std::vector<GLfloat> colShop(int n, int kind = 0);		// color shop
	static std::vector<GLfloat> alpShop(int n, int kind = 0);		// alpha shop
public:
	static BangInfo orderBang(int n) {
		BangInfo ret;
		ret.vPool = velShop(n);
		ret.XYAPool = angShop(n, 2); // we see the XY-plane
		ret.XZAPool = angShop(n);

		ret.RcrPool = colShop(n);
		ret.GcrPool = colShop(n);
		ret.BcrPool = colShop(n);

		ret.AcrPool = alpShop(n);
		return ret;
	}
};

class Particle {
private:
	GLfloat x;
	GLfloat y;
	GLfloat z;

	GLfloat xSpeed;
	GLfloat ySpeed;
	GLfloat zSpeed;

	GLfloat R, G, B, A;
	GLfloat Rcr, Gcr, Bcr, Acr; // change rate of RGB and Alpha
	GLfloat psize;

	// decided by environtment
	GLfloat xAcce;	// by drag force
	GLfloat yAcce;	// by gravity
	GLfloat zAcce;	// by drag force
public:
	Particle() {
		xAcce = 0;
		yAcce = GRAVITY;
		zAcce = 0;
		reset(0);
	}
	inline void reset(GLfloat s) {
		x = y = z = 0;
		xSpeed = ySpeed = zSpeed = 0;

		R = G = B = A = 0;
		Rcr = Gcr = Bcr = Acr = 0;
		psize = s;
	}
	inline void setPosition(GLfloat x, GLfloat y, GLfloat z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}
	inline void setSpeed(GLfloat xSpeed, GLfloat ySpeed, GLfloat zSpeed) {
		this->xSpeed = xSpeed;
		this->ySpeed = ySpeed;
		this->zSpeed = zSpeed;
	}
	inline void setApp(GLfloat red, GLfloat green, GLfloat blue,
			GLfloat alpha) {
		this->R = red;
		this->G = green;
		this->B = blue;
		this->A = alpha;
	}

	// bang function
	inline void bangSpeed(GLfloat v, GLfloat XZAngle, GLfloat XYAngle) {

		this->xSpeed += v * std::cos(XZAngle*PI) * std::cos(XYAngle*PI);
		this->ySpeed += v * std::cos(XZAngle*PI) * std::sin(XYAngle*PI);
		this->zSpeed += v * std::sin(XZAngle*PI);
	}
	inline void bangApp(GLfloat Rcr, GLfloat Gcr, GLfloat Bcr, GLfloat Acr) {
		this->Rcr = Rcr;
		this->Gcr = Gcr;
		this->Bcr = Bcr;

		this->Acr = Acr;
	}
	inline bool isVisible() const {
		return A > 0;
	}
	inline bool isRising() const {
		return ySpeed < 0;
	}
	void draw() const {
		// Set the point size of the firework particles (this needs to be called BEFORE opening the glBegin(GL_POINTS) section!)
		glPointSize(this->psize);
		glBegin(GL_POINTS);
		// Set colour to yellow on the way up, then whatever colour firework should be when exploded
		glColor4f(R, G, B, A);
		// Draw the point
		glVertex2f(x, y);
		glEnd();
	}
	void move() {
		x += xSpeed;
		y += ySpeed;
		z += zSpeed;

		// Drage force
		xSpeed += (xAcce - fabs(xSpeed)*xSpeed* (psize) * DRAG_RO);
		ySpeed += (yAcce - fabs(ySpeed)*ySpeed* (psize) * DRAG_RO);
		zSpeed += (zAcce - fabs(zSpeed)*zSpeed* (psize) * DRAG_RO);

		R += Rcr;
		G += Gcr;
		B += Bcr;
		A += Acr;
	}
};

class Firework {
private:
	void initialise();
	void bang();
	// internal status
	GLboolean hasExploded;
	std::vector<Particle> particles;
	// parameters
	GLint framesUntilLaunch;

public:
	// Object member functions
	Firework(int pnum = 100); // Constructor declaration
	void move();
	void draw();
	inline bool isVisible() const {
		bool ret = false;
		for (int i = 0; i < particles.size(); i++) {
			if (particles[i].isVisible()) {
				ret = true;
				break;
			}
		}
		return ret;
	}
	inline bool shouldBang() const {
		bool ret = true;
		for (int i = 0; i < particles.size(); i++) {
			if (particles[i].isRising()) {
				ret = false;
				break;
			}
		}
		return ret;
	}
};
#endif
