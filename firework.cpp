#include "Firework.hpp"

const GLfloat DRAG_RO = 0.0004f;
const GLfloat GRAVITY = 0.05f;
const GLfloat minXSpeed = -1.f;
const GLfloat maxXSpeed = 1.f;
const GLfloat minYSpeed = -12.f;	// negative -> particle should move upward inintially
const GLfloat maxYSpeed = -15.f;
const GLfloat maxSIZE = 8;
const GLfloat minSIZE = 4;
const GLfloat PI = 3.14157265;

int sampleInt(int n) {
	// sample from [1,n]
	return rand() % n + 1;
}
GLfloat sampleFrom(GLfloat a, GLfloat b) {
	if (a > b)
		std::swap(a, b);
	return a + (rand() / (float) RAND_MAX) * (b - a);
}
std::vector<GLfloat> sampleFrom(int n, GLfloat a, GLfloat b) {
	std::vector<GLfloat> ret(n);
	for (int i = 0; i < n; i++) {
		ret[i] = sampleFrom(a, b);
	}
	return ret;
}

std::vector<GLfloat> BangFactory::velShop(int n, int kind) {
	const GLfloat base_vel = 1.0f;
	const GLfloat perturb_vel = 0.2f;
	const GLfloat gap_vel = 0.8f;
	// velocity shop
	std::vector<GLfloat> ret;
	const int KIND_N = 3;
	if (kind == 0) {
		kind = sampleInt(KIND_N);
	}
	switch (kind) {
	case 1: {
		std::vector<GLfloat> ret1 = sampleFrom(n / 2, base_vel+gap_vel, base_vel+1+perturb_vel);
		std::vector<GLfloat> ret2 = sampleFrom(n - n / 2, base_vel, base_vel+perturb_vel);
		ret.reserve(n);
		ret.insert(ret.end(), ret1.begin(), ret1.end());
		ret.insert(ret.end(), ret2.begin(), ret2.end());
	}
		break;
	case 2: {
		std::vector<GLfloat> ret1 = sampleFrom(n / 3, base_vel+gap_vel, base_vel+gap_vel+perturb_vel);
		std::vector<GLfloat> ret2 = sampleFrom(n / 3, base_vel, base_vel+perturb_vel);
		std::vector<GLfloat> ret3 = sampleFrom(n - (n / 3) * 2, base_vel-gap_vel, base_vel-gap_vel+perturb_vel);
		ret.reserve(n);
		ret.insert(ret.end(), ret1.begin(), ret1.end());
		ret.insert(ret.end(), ret2.begin(), ret2.end());
		ret.insert(ret.end(), ret3.begin(), ret3.end());
	}
		break;
	default: {
		ret = sampleFrom(n, base_vel, base_vel+perturb_vel); // must be positive
	}
	}
	return ret;
}
std::vector<GLfloat> BangFactory::angShop(int n, int kind) {
	// angle shop
	std::vector<GLfloat> ret;
	const int KIND_N = 2;
	if (kind == 0) {
		kind = sampleInt(KIND_N);
	}
	switch (kind) {
	case 1: {
		ret = std::vector<GLfloat>(n, 0);
	}
		break;
	default: {
		ret = sampleFrom(n, -1.0f, 1.0f);
	}
	}
	return ret;
}
std::vector<GLfloat> BangFactory::colShop(int n, int kind) { // color shop
	std::vector<GLfloat> ret;
	const int KIND_N = 5;
	if (kind == 0) {
		kind = sampleInt(KIND_N);
	}
	switch (kind) {
	case 1: {
		std::vector<GLfloat> ret1 = sampleFrom((n + 1) / 2, -0.01f, 0.01f);
		ret = std::vector<GLfloat>(n);
		for (int i = 0; i < n; i++) {
			if (i < (n + 1) / 2) {
				ret[i] = ret1[i];
			} else {
				ret[i] = -ret1[i - (n + 1) / 2];
			}
		}
	}
		break;
	case 2: {
		GLfloat cr = sampleFrom(-0.01f, 0.01f);
		ret = std::vector<GLfloat>(n, cr);
	}
		break;
	case 3: {
		ret = std::vector<GLfloat>(n, 0); // do not change color
	}
		break;
	case 4: {
		GLfloat cr = sampleFrom(-0.01f, 0.01f);
		ret = std::vector<GLfloat>(n, cr);
		for (int i = 0; i < n / 2; i++) {
			ret[i] = -ret[i];
		}
	}
		break;
	default: {
		ret = sampleFrom(n, -0.01f, 0.01f);
	}
		break;
	}
	return ret;
}
std::vector<GLfloat> BangFactory::alpShop(int n, int kind) { // alpha shop
	std::vector<GLfloat> ret;
	const int KIND_N = 2;
	const GLfloat base_alpha = -0.02f;
	const GLfloat perturb_alpha = 0.002f;
	const GLfloat gap_alpha = 0.008f;
	if (kind == 0) {
		kind = sampleInt(KIND_N);
	}
	switch (kind) {
	case 1: {
		ret = sampleFrom(n, base_alpha, base_alpha+perturb_alpha);
	}
		break;
	default: {
		ret = sampleFrom(n, base_alpha+gap_alpha, base_alpha+gap_alpha+perturb_alpha);
	}
	}
	return ret;
}

// Constructor implementation
Firework::Firework(int pnum) {
	// We call a function to perform the constructor's job here so that we can re-initialise the same firework
	// later on without having to destroy the object and recreate it!
	this->particles = std::vector<Particle>(pnum);
	initialise();
}

void Firework::initialise() {
	// Pick an initial x location and random x/y speeds for each particle making up the firework
	// Note: Each particle in the firework must have the exact same values for the firework to rise as a single point!
	float xLoc = sampleFrom(windowWidth / 3, 2 * windowWidth / 3);
	float xSpeedVal = sampleFrom(minXSpeed, maxXSpeed);
	float ySpeedVal = sampleFrom(minYSpeed, maxYSpeed);
	//cout << ySpeedVal << endl;

	// Assign a random colour and full alpha (i.e. particle is completely opaque)
	GLfloat red = sampleFrom(0, 1);
	GLfloat green = sampleFrom(0, 1);
	GLfloat blue = sampleFrom(0, 1);
	GLfloat alpha = 1.0f;
	GLfloat psize = sampleFrom(minSIZE, maxSIZE);
	// Set initial x/y location and speeds for each particle in the firework
	for (int i = 0; i < particles.size(); i++) {
		particles[i].reset(psize);
		particles[i].setPosition(xLoc, windowHeight + 10.0f, 0);
		particles[i].setSpeed(xSpeedVal, ySpeedVal, 0);
		particles[i].setApp(red, green, blue, alpha);
	}

	// Firework will launch after a random amount of frames between 0 and 400
	framesUntilLaunch = (int) sampleFrom(0, 400);
	// Flag to keep trackof whether the firework has exploded or not
	hasExploded = false;
	//cout << "Initialised a firework." << endl;
}
// Function to control bang effect
void Firework::bang() {
	// Once a fireworks speed turns positive (i.e. at top of arc) - blow it up!
	BangInfo bi = BangFactory::orderBang(particles.size());
	std::vector<GLfloat> vPool = bi.vPool;
	std::vector<GLfloat> XYAPool = bi.XYAPool;
	std::vector<GLfloat> XZAPool = bi.XZAPool;

	std::vector<GLfloat> RcrPool = bi.RcrPool;
	std::vector<GLfloat> GcrPool = bi.GcrPool;
	std::vector<GLfloat> BcrPool = bi.BcrPool;
	std::vector<GLfloat> AcrPool = bi.AcrPool;

	for (int i = 0; i < particles.size(); i++) {
		// Set a random x and y speed beteen [-4,-3] and + [3,4]
		particles[i].bangSpeed(vPool[i], XZAPool[i], XYAPool[i]);
		particles[i].bangApp(RcrPool[i], GcrPool[i], BcrPool[i], AcrPool[i]);
	}
	// Set the firework hasExploded flag to true
	hasExploded = true;
}
// Function to move a firework
void Firework::move() {
	// Once the firework is ready to launch start moving the particles
	if (framesUntilLaunch <= 0) {
		for (int i = 0; i < particles.size(); i++) {
			particles[i].move();
		}
	}
	if (!hasExploded) { // before bang
		framesUntilLaunch--; // Decrease the launch countdown clock!
		// Once a fireworks speed turns positive (i.e. at top of arc) - blow it up!
		if (shouldBang()) {
			bang();
		}
	} else { // after bang
		if (!isVisible()) {
			initialise();
		}
	}
}

void Firework::draw() {
	for (int i = 0; i < particles.size(); i++) {
		particles[i].draw();
	}
}
