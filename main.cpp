#include <iostream>
#include <ctime>
#include <string>
#include <GLFW/glfw3.h>    // Include OpenGL Framework library
#include <OPENGL/glext.h>
#define GL_RGBA32F GL_RGBA32F_ARB

#include "glInfo.h"
#include "events.hpp"	   // control keyboard, mouse events
#include "Firework.hpp"    // Include our fireworks class

using namespace std;

// FBO utils
bool checkFramebufferStatus();

// constants
const char windowName[] = "Fireworks";
const GLint FIREWORKS = 15; // Number of fireworks
const GLint NRBUF = 2;
const GLfloat blend_weight = 0.8;

// global variables
GLuint fboId;                       // ID of FBO
GLuint textureId;                   // ID of texture
GLuint rboId[NRBUF];                       // ID of Renderbuffer object

bool fboSupported;
bool fboUsed;

GLint windowWidth = 1280; // Define our window width
GLint windowHeight = 800; // Define our window height
GLint frameCount = 0;     // Keep track of how many frames we've drawn
// Create our array of fireworks
Firework fw[FIREWORKS];

//========================================================================
// Print errors
//========================================================================

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

// Function to set some initial OpenGL state-machine properties
void initGL() {
	// Use Gouraud (smooth) shading
	glShadeModel(GL_SMOOTH);
	glfwSwapInterval(1); // Lock to vertical sync of monitor (normally 60Hz, so 60fps)

	// ----- Window and Projection Settings -----
	// Setup our viewport to be the entire size of the window
	glViewport(0, 0, (GLsizei) windowWidth, (GLsizei) windowHeight);

	// Change to the projection matrix, reset the matrix and set up orthagonal projection (i.e. 2D)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, windowWidth, windowHeight, 0, 0, 1); // Parameters: left, right, bottom, top, near, far

	//  Enable smooth shading (i.e. gouraud shading)
	glShadeModel(GL_SMOOTH);

	// Set our clear colour to opaque black
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);


	glEnable(GL_POINT_SMOOTH); // Smooth the points so that they're circular and not square
}

// Function to draw our OpenGL scene
void drawScene(GLFWwindow* window) {

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// Set ModelView matrix mode and reset to the default identity matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Displacement trick for exact pixelisation
	glTranslatef(0.375, 0.375, 0);

	// Draw our fireworks
	for (int loop = 0; loop < FIREWORKS; loop++) {
		// draw a firework
		fw[loop].draw();
		fw[loop].move();
	}
} // End of drawScene function

// Our main function
int main() {
	srand((unsigned) time(NULL)); // Seed the random number generator
    GLFWwindow* window;
    GLenum gl_error;
    glInfo glInfo;
    
    glfwSetErrorCallback(error_callback);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	window = glfwCreateWindow(windowWidth, windowHeight, windowName, NULL,
	NULL);
	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	// Initialise glfw
	glfwSetKeyCallback(window, key_callback);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetCursorPosCallback(window, cursor_position_callback);
	glfwSetScrollCallback(window, scroll_callback);

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	glfwGetFramebufferSize(window, &windowWidth, &windowHeight);
	framebuffer_size_callback(window, windowWidth, windowHeight);

    // Initialize OpenGL
	initGL();
    
    glInfo.getInfo();
    glInfo.printSelf();

    // create a texture object
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, windowWidth, windowHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE); // automatic mipmap generation included in OpenGL v1.4
    if ((gl_error = glGetError()) != GL_NO_ERROR) {
        cerr << "BlitFramebuffer: [" << gl_error << "] "
        << gluErrorString(gl_error) << endl;
        exit(-1);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    
    if(glInfo.isExtensionSupported("GL_ARB_framebuffer_object"))
    {
        fboSupported = fboUsed = true;
        std::cout << "Video card supports GL_ARB_framebuffer_object." << std::endl;
    }
    else
    {
        fboSupported = fboUsed = false;
        std::cout << "Video card does NOT support GL_ARB_framebuffer_object." << std::endl;
    }
    
    if(fboSupported)
    {
        // create a framebuffer object, you need to delete them when program exits.
        glGenFramebuffers(1, &fboId);
        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        
        
        glGenRenderbuffers(NRBUF, rboId);
        glBindRenderbuffer(GL_RENDERBUFFER, rboId[0]);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_RGBA32F,windowWidth, windowHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_RENDERBUFFER, rboId[0]);
        
        glBindRenderbuffer(GL_RENDERBUFFER, rboId[1]);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, windowWidth, windowHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboId[1]);
        
        // attach a texture to FBO color attachement point
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
        
        // check FBO status
        bool status = checkFramebufferStatus();
        if(!status)
            fboUsed = false;
        
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, fboId);
    glDrawBuffer(GL_COLOR_ATTACHMENT1);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    while (!glfwWindowShouldClose(window)) {
		// Draw our scene
        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        drawScene(window);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        
        glBindTexture(GL_TEXTURE_2D, textureId);
//        glGenerateMipmap(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, 0);

        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glDrawBuffer(GL_COLOR_ATTACHMENT1);
        glPushAttrib(GL_ENABLE_BIT);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);
        glEnable(GL_BLEND);
        glBlendColor(0.0, 0.0, 0.0, blend_weight);
        glBlendFunc(GL_SRC_ALPHA, GL_CONSTANT_ALPHA);
        glBlendEquation(GL_FUNC_ADD);
        glEnable(GL_TEXTURE_2D);
        
        glBindTexture(GL_TEXTURE_2D, textureId);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glBegin(GL_QUADS);
            glTexCoord2f(0,0);
            glVertex2f( 0.0f, windowHeight);
            glTexCoord2f(1,0);
            glVertex2f( windowWidth, windowHeight);
            glTexCoord2f(1,1);
            glVertex2f( windowWidth, 0.0f);
            glTexCoord2f(0,1);
            glVertex2f( 0.0f, 0.0f);
        glEnd();

        glBindTexture(GL_TEXTURE_2D, 0);
        glPopAttrib();

        // now blit the "accumulation buffer" to default framebuffer
        glBindFramebuffer(GL_READ_FRAMEBUFFER, fboId);
        glReadBuffer(GL_COLOR_ATTACHMENT1);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        glBlitFramebuffer(0, 0, windowWidth, windowHeight,
                          0, 0, windowWidth, windowHeight,
                          GL_COLOR_BUFFER_BIT, GL_NEAREST);
        
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
		// Increase our frame counter
		frameCount++;
        glfwSwapBuffers(window); // Swap the buffers to display the scene (so we don't have to watch it being drawn!)
		glfwPollEvents();
	}
    
	glfwDestroyWindow(window);
    glDeleteRenderbuffers(NRBUF, rboId);
    glDeleteFramebuffers(1, &fboId);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}

///////////////////////////////////////////////////////////////////////////////
// check FBO completeness
///////////////////////////////////////////////////////////////////////////////
bool checkFramebufferStatus()
{
    // check FBO status
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    switch(status)
    {
        case GL_FRAMEBUFFER_COMPLETE:
            std::cout << "Framebuffer complete." << std::endl;
            return true;
            
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            std::cout << "[ERROR] Framebuffer incomplete: Attachment is NOT complete." << std::endl;
            return false;
            
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            std::cout << "[ERROR] Framebuffer incomplete: No image is attached to FBO." << std::endl;
            return false;
            /*
             case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
             std::cout << "[ERROR] Framebuffer incomplete: Attached images have different dimensions." << std::endl;
             return false;
             
             case GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
             std::cout << "[ERROR] Framebuffer incomplete: Color attached images have different internal formats." << std::endl;
             return false;
             */
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
            std::cout << "[ERROR] Framebuffer incomplete: Draw buffer." << std::endl;
            return false;
            
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
            std::cout << "[ERROR] Framebuffer incomplete: Read buffer." << std::endl;
            return false;
            
        case GL_FRAMEBUFFER_UNSUPPORTED:
            std::cout << "[ERROR] Framebuffer incomplete: Unsupported by FBO implementation." << std::endl;
            return false;
            
        default:
            std::cout << "[ERROR] Framebuffer incomplete: Unknown error." << std::endl;
            return false;
    }
}
